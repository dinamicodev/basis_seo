<?php
/**
 * @file
 * basis_seo.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function basis_seo_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_user';
  $strongarm->value = 0;
  $export['metatag_enable_user'] = $strongarm;

  return $export;
}
